# example-ansible-jinja2

## Execution

## 01-Jinja2-If
        ansible-playbook -i hosts.yml 01-Jinja2-If/jinja2_playbook.yml

## 02-Jinja2-elif
        ansible-playbook -i hosts.yml 02-Jinja2-elif/jinja2_playbook.yml

## 03-Jinja2-elif-else
        ansible-playbook -i hosts.yml 03-Jinja2-elif-else/jinja2_playbook.yml

## 04-Jinja2-variable-not-defined
        ansible-playbook -i hosts.yml 04-Jinja2-variable-not-defined/jinja2_playbook.yml

## 05-Jinja2-variable-defined
        ansible-playbook -i hosts.yml 05-Jinja2-variable-defined/jinja2_playbook.yml

## 06-Jinja2-for-loop
        ansible-playbook -i hosts.yml 06-Jinja2-for-loop/jinja2_playbook.yml

## 07-Jinja2-range-in-for-loop
        ansible-playbook -i hosts.yml 07-Jinja2-range-in-for-loop/jinja2_playbook.yml

## 08-Jinja2-template
        ansible-playbook -i hosts.yml 08-Jinja2-template/jinja2_playbook.yml

## git cheatsheet

Cloning

    git clone https://gitlab.com/imjoebond/example-ansible-jinja2.git

or if you have ssh keys defined in the repository provider

    git clone git@gitlab.com:imjoebond/example-ansible-jinja2.git

Branching

    git branch jb_test_branch
    git checkout jb_test_branch
    git push --set-upstream origin jb_test_branch
    
  

Commiting & Pushing
        
        git add path/filename.txt
        git commit -m "commit message"
        git push